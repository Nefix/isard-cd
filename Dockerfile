#
# Build phase
#
FROM golang:1.16-alpine as build

RUN apk add --no-cache \
    git \
    make

WORKDIR /build

COPY go.mod /build
COPY go.sum /build

RUN go mod download

WORKDIR /

COPY . /build

WORKDIR /build

RUN go build -o isard-cd


#
# Isard CD
#
FROM alpine:3.13

RUN apk add --no-cache \
    git \
    make \
    go \
    docker \
    docker-compose

COPY --from=build /build/isard-cd /isard-cd

EXPOSE 8000

CMD [ "/isard-cd" ]
