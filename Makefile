all: build docker

tidy:
	go mod tidy

build: tidy
	go build -o isard-cd

docker:
	docker build -t isard/isardvdi-cd .

run:
	docker run \
		-p 80:80 \
		-p 443:443 \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-v $(PWD)/certs:/root/.local/share/certmagic \
		-v $(PWD)/deploys:/tmp \
		isard/isardvdi-cd \
		/isard-cd -gandi-token=$(gandi-token)
