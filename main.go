package main

import (
	"bytes"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/caddyserver/certmagic"
	"github.com/compose-spec/compose-go/cli"
	"github.com/compose-spec/compose-go/types"
	dockerTypes "github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
	"github.com/libdns/gandi"
	"gitlab.com/nefix/isard-cd/deployments"
	"gopkg.in/yaml.v2"
)

const (
	newTypeMR     = "mr"
	newTypeBranch = "branch"
)

var baseHost = "cd.isardvdi.com"

func getDeployment(r *http.Request) (string, int, error) {
	baseHost := strings.Split(baseHost, ".")
	host := strings.Split(r.Host, ":")
	var port int
	if len(host) == 2 {
		i, err := strconv.Atoi(host[1])
		if err != nil {
			return "", 0, fmt.Errorf("invalid port: %w", err)
		}

		port = i
	} else {
		switch r.URL.Scheme {
		case "http", "":
			port = 80
		case "https":
			port = 443
		}
	}

	host = strings.Split(host[0], ".")
	if len(host) != len(baseHost)+1 {
		return "", 0, fmt.Errorf("invalid host")
	}

	for i, h := range baseHost {
		if host[i+1] != h {
			return "", 0, fmt.Errorf("invalid host")
		}
	}

	return host[0], port, nil
}

type newRequest struct {
	Type string `json:"type,omitempty"`

	TargetRepo   string `json:"target_repo,omitempty"`
	TargetBranch string `json:"target_branch,omitempty"`

	MRSourceRepo   string `json:"mr_source_repo,omitempty"`
	MRSourceCommit string `json:"mr_source_commit,omitempty"`
	MRIID          string `json:"mr_iid,omitempty"`
}

type deleteRequest struct {
	Name string `json:"name,omitempty"`
}

func main() {
	var gandiToken string

	flag.StringVar(&baseHost, "base-host", "cd.isardvdi.com", "base host of the CD service")
	flag.StringVar(&gandiToken, "gandi-token", "", "gandi API token for certs")
	flag.Parse()
	if gandiToken == "" {
		log.Fatal("no gandi token set. Use the -gandi-token flag")
	}

	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		panic(err)
	}
	deployments := deployments.New(ctx, cli)

	mux := http.NewServeMux()
	mux.HandleFunc("/new", func(w http.ResponseWriter, r *http.Request) {
		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer r.Body.Close()

		req := &newRequest{}
		if err := json.Unmarshal(b, req); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		var opts *newOpts
		switch req.Type {
		case newTypeMR:
			opts = &newOpts{
				newType:      newTypeMR,
				sourceRepo:   req.MRSourceRepo,
				sourceCommit: req.MRSourceCommit,
				targetRepo:   req.TargetRepo,
				targetBranch: req.TargetBranch,
			}

		case newTypeBranch:
			opts = &newOpts{
				newType:      newTypeBranch,
				targetRepo:   req.TargetRepo,
				targetBranch: req.TargetBranch,
			}

		default:
			http.Error(w, "unknown request type", http.StatusInternalServerError)
		}

		name, project, err := newDeployment(opts)
		if err != nil {
			log.Println(opts, err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		oldProject := deployments.Get(name)

		deployments.Set(name, project)

		if oldProject != nil {
			if err := deleteDeployment(oldProject); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		}

		w.WriteHeader(http.StatusOK)
	})

	mux.HandleFunc("/delete", func(w http.ResponseWriter, r *http.Request) {
		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer r.Body.Close()

		req := &deleteRequest{}
		if err := json.Unmarshal(b, req); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		project := deployments.Get(req.Name)
		if project == nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		if err := deleteDeployment(project); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		deployments.Delete(req.Name)
	})

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		name, port, err := getDeployment(r)
		if err != nil {
			http.Error(w, fmt.Errorf("get deployment: %w", err).Error(), http.StatusInternalServerError)
		}

		deployment := deployments.Get(name)
		if deployment == nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		id := strings.TrimSuffix(strings.TrimPrefix(deployment.WorkingDir, "/tmp/"), "/deployments/docker-compose")

		containers, err := cli.ContainerList(ctx, dockerTypes.ContainerListOptions{
			Filters: filters.NewArgs(filters.Arg("name", "^"+id+".*$")),
		})
		if err != nil {
			http.Error(w, fmt.Errorf("list container deployments: %w", err).Error(), http.StatusInternalServerError)
		}

		var proxyTarget string
		for _, c := range containers {
			for _, p := range c.Ports {
				if p.PrivatePort == uint16(port) {
					proxyTarget = c.NetworkSettings.Networks["default"].IPAddress + ":" + strconv.Itoa(port)
				}
			}

			if c.Names[0] == "/"+id+"_backend_1" {
				proxyTarget = c.NetworkSettings.Networks[id].IPAddress + ":1312"
			}
		}

		if proxyTarget == "" {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		scheme := r.URL.Scheme
		if r.URL.Scheme == "https" || r.URL.Scheme == "" {
			scheme = "http"
		}
		director := func(req *http.Request) {
			req.Header.Add("X-Forwarded-Host", req.Host)
			req.Header.Add("X-Origin-Host", proxyTarget)
			req.URL.Scheme = scheme
			req.URL.Host = proxyTarget
		}

		proxy := &httputil.ReverseProxy{Director: director}
		proxy.ServeHTTP(w, r)
	})

	certmagic.DefaultACME.Agreed = true
	certmagic.DefaultACME.DNS01Solver = &certmagic.DNS01Solver{
		DNSProvider: &gandi.Provider{
			APIToken: gandiToken,
		},
	}

	log.Println("Isard CD is running!")
	if err := certmagic.HTTPS([]string{baseHost, "*." + baseHost}, mux); err != nil {
		panic(err)
	}
}

type newOpts struct {
	newType      string
	targetRepo   string
	targetBranch string
	sourceRepo   string
	sourceCommit string
	mergeIID     string
}

func newDeployment(opts *newOpts) (string, *types.Project, error) {
	dir, err := os.Getwd()
	if err != nil {
		return "", nil, fmt.Errorf("get working directory: %w", err)
	}
	defer func() {
		if err := os.Chdir(dir); err != nil {
			panic(fmt.Errorf("get back to original working directory: %w", err))
		}
	}()

	tmp, err := ioutil.TempDir("", "isard-cd-")
	if err != nil {
		return "", nil, fmt.Errorf("create temp dir: %w", err)
	}

	if err := os.Chdir(tmp); err != nil {
		return "", nil, fmt.Errorf("change working dir to temp dir: %w", err)
	}

	if b, err := exec.Command("git", "clone", opts.targetRepo, ".").CombinedOutput(); err != nil {
		return "", nil, fmt.Errorf("clone repository: %s, %w", b, err)
	}

	var name string
	if opts.newType == newTypeMR {
		if b, err := exec.Command("git", "remote", "add", "source", opts.sourceRepo).CombinedOutput(); err != nil {
			return "", nil, fmt.Errorf("add remote: %s, %w", b, err)
		}

		if b, err := exec.Command("git", "fetch", "source").CombinedOutput(); err != nil {
			return "", nil, fmt.Errorf("fetch remote: %s, %w", b, err)
		}

		cmd1 := exec.Command("git", "ls-remote")
		cmd2 := exec.Command("grep", opts.sourceCommit + ".*head")
		cmd3 := exec.Command("awk", "{ print $2 }")

		var b bytes.Buffer

		cmd2.Stdin, err = cmd1.StdoutPipe()
		if err != nil {
			return "", nil, fmt.Errorf("get MR name: 1st pipe: %w", err)
		}
		cmd3.Stdin, err = cmd2.StdoutPipe()
		if err != nil {
			return "", nil, fmt.Errorf("get MR name: 2nd pipe: %w", err)
		}
		cmd3.Stdout = &b

		if err := cmd3.Start(); err != nil {
			return "", nil, fmt.Errorf("get MR name: 1st start: %w", err)
		}

		if err := cmd2.Start(); err != nil {
			return "", nil, fmt.Errorf("get MR name: 2nd start: %w", err)
		}

		if err := cmd1.Run(); err != nil {
			return "", nil, fmt.Errorf("get MR name: 1st run: %w", err)
		}

		if err := cmd2.Wait(); err != nil {
			return "", nil, fmt.Errorf("get MR name: 1st wait: %w", err)
		}

		if err := cmd3.Wait(); err != nil {
			fmt.Println(b.String())
			return "", nil, fmt.Errorf("get MR name: 2nd wait: %w", err)
		}

		name = strings.TrimSuffix(strings.TrimPrefix(strings.TrimSpace(b.String()), "refs/merge-requests/"), "/head")

	} else {
		name = opts.targetBranch
	}

	if b, err := exec.Command("git", "checkout", "origin/"+opts.targetBranch).CombinedOutput(); err != nil {
		return "", nil, fmt.Errorf("checkout branch: %s, %w", b, err)
	}

	if opts.newType == newTypeMR {
		if b, err := exec.Command("git", "merge", opts.sourceCommit).CombinedOutput(); err != nil {
			return "", nil, fmt.Errorf("merge changes: %s, %w", b, err)
		}
	}

	compOpts, err := cli.NewProjectOptions([]string{"deployments/docker-compose/docker-compose.yml"}, cli.WithEnv([]string{"VERSION=" + name}))
	if err != nil {
		return "", nil, fmt.Errorf("load docker-compose options: %w", err)
	}

	project, err := cli.ProjectFromOptions(compOpts)
	if err != nil {
		return "", nil, fmt.Errorf("load docker-compose: %w", err)
	}

	for i := range project.Services {
		project.Services[i].Ports = []types.ServicePortConfig{}
		project.Services[i].Volumes = []types.ServiceVolumeConfig{}

		if strings.HasPrefix(project.Services[i].Image, "isard/") {
			project.Services[i].Image = "registry.gitlab.com/isard/isardvdi/" + project.Services[i].Image
		}
	}
	net := project.Networks["default"]
	net.Name = strings.TrimPrefix(tmp, "/tmp/")
	project.Networks["default"] = net

	b, err := yaml.Marshal(project)
	if err != nil {
		return "", nil, fmt.Errorf("marshal docker-compose: %w", err)
	}

	if err := ioutil.WriteFile("deployments/docker-compose/docker-compose.yml", b, os.FileMode(os.O_WRONLY)); err != nil {
		return "", nil, fmt.Errorf("update docker-compose: %w", err)
	}

	if b, err := exec.Command("make", "docker-compose-up").CombinedOutput(); err != nil {
		return "", nil, fmt.Errorf("start deployment: %s, %w", b, err)
	}

	time.Sleep(10 * time.Second)

	if b, err := exec.Command("go", "run", "cli/cmd/cli/main.go", net.Name).CombinedOutput(); err != nil {
		return "", nil, fmt.Errorf("start deployment: %s, %w", b, err)
	}

	if err := ioutil.WriteFile(".isard-cd.cfg", []byte(name), 0755); err != nil {
		return "", nil, fmt.Errorf("save deployment: %s, %w", b, err)
	}

	return name, project, nil
}

func deleteDeployment(project *types.Project) error {
	dir, err := os.Getwd()
	if err != nil {
		return fmt.Errorf("get working directory: %w", err)
	}

	tmp := filepath.Join(project.WorkingDir, "../../")
	if err := os.Chdir(tmp); err != nil {
		return fmt.Errorf("change working dir to temp dir: %w", err)
	}

	if b, err := exec.Command("make", "docker-compose-down").CombinedOutput(); err != nil {
		return fmt.Errorf("stop deployment: %s, %w", b, err)
	}

	if err := os.Chdir(dir); err != nil {
		return fmt.Errorf("get back to original working directory: %w", err)
	}

	if err := os.RemoveAll(tmp); err != nil {
		return fmt.Errorf("clean tmp directory: %w", err)
	}

	return nil
}
