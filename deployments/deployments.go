package deployments

import (
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"sync"

	composeCli "github.com/compose-spec/compose-go/cli"
	"github.com/compose-spec/compose-go/types"
	dockerTypes "github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
)

type Deployments struct {
	m   map[string]*types.Project
	mux sync.Mutex
}

func New(ctx context.Context, cli *client.Client) *Deployments {
	d := &Deployments{
		m: map[string]*types.Project{},
	}

	containers, err := cli.ContainerList(ctx, dockerTypes.ContainerListOptions{
		Filters: filters.NewArgs(filters.Arg("name", "^.*isard-cd-.*$")),
	})
	if err != nil {
		panic(fmt.Errorf("list existing deployments: %w", err))
	}

	for _, c := range containers {
		if strings.HasPrefix(c.Names[0], "/isard-cd-") {
			id := strings.Split(c.Names[0], "_")[0][1:]

			dir, err := os.Getwd()
			if err != nil {
				panic(fmt.Errorf("get working directory: %w", err))
			}

			tmp := filepath.Join("/tmp", id)
			if err := os.Chdir(tmp); err != nil {
				panic(fmt.Errorf("change working dir to temp dir: %w", err))
			}

			f, err := os.Open(filepath.Join(tmp, ".isard-cd.cfg"))
			if err != nil {
				continue
			}

			b, err := ioutil.ReadAll(f)
			if err != nil {
				panic(fmt.Errorf("read isard-cd config file: %w", err))
			}

			if d.Get(string(b)) != nil {
				continue
			}

			compOpts, err := composeCli.NewProjectOptions([]string{"deployments/docker-compose/docker-compose.yml"})
			if err != nil {
				panic(fmt.Errorf("load docker-compose options: %w", err))
			}

			project, err := composeCli.ProjectFromOptions(compOpts)
			if err != nil {
				panic(fmt.Errorf("load docker-compose: %w", err))
			}

			d.Set(string(b), project)

			if err := os.Chdir(dir); err != nil {
				panic(fmt.Errorf("get back to original working directory: %w", err))
			}
		}
	}

	return d
}

func (d *Deployments) Get(name string) *types.Project {
	d.mux.Lock()
	defer d.mux.Unlock()

	project, ok := d.m[name]
	if !ok {
		return nil
	}

	return project
}

func (d *Deployments) Set(name string, project *types.Project) {
	d.mux.Lock()
	defer d.mux.Unlock()

	d.m[name] = project
}

func (d *Deployments) Delete(name string) {
	d.mux.Lock()
	defer d.mux.Unlock()

	delete(d.m, name)
}
